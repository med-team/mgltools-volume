#######################################################################

### This file contains Distutil setup() for building Volume package
### distribution :

###########################################################################

from distutils.core import setup, Extension
from distutils.command.sdist import sdist
from distutils.command.install_data import install_data
from distutils.command.build import build

import os, sys
from os import path

platform = sys.platform



# Had to overwrite the prune_file_list method of sdist to not
# remove automatically the CVS directories from the distribution.


class modified_sdist(sdist):
    def prune_file_list(self):
        """
        """
        build = self.get_finalized_command('build')
        base_dir = self.distribution.get_fullname()
        self.filelist.exclude_pattern(None, prefix=build.build_base)
        self.filelist.exclude_pattern(None, prefix=base_dir)

# Overwrite the run method of the install_data to install the data files
# in the package.
class modified_install_data(install_data):

    def run(self):
        install_cmd = self.get_finalized_command('install')
        self.install_dir = getattr(install_cmd, 'install_lib')
        #print "install_dir", self.install_dir
        return install_data.run(self)


# List of the python packages to be included in this distribution.
# sdist does not go recursively into subpackages so they need to be
# explicitly listed.
# From these packages only the python modules will be taken
packages = ['Volume',
            'Volume.IO',
            'Volume.IO.Tests',
            'Volume.IO.Mesh', 
            'Volume.Operators',
            'Volume.Operators.Tests',
            #'Volume.Pvv',
            'Volume.Renderers',
            'Volume.Renderers.UTVolumeLibrary',
            'Volume.Renderers.UTVolumeLibrary.DejaVu',
            #'Volume.Renderers.VLI',
            #'Volume.Renderers.VLI.DejaVu',
            #'Volume.Tests',
            'Volume.VisionInterface','Volume.VisionInterface.Tests',
            ]
        
# List of the other (not python) files included in the distribution .
# MANIFEST.in lists these files, so they are included in the distribution.

data_files = []

def getDataFiles(file_list, directory, names):
    fs = []
    for name in names:
        ext = os.path.splitext(name)[1]
        #print directory, name, ext, len(ext)
        if ext !=".py" and ext !=".pyc":
            fullname = os.path.join(directory,name)
            if not os.path.isdir(fullname):
                fs.append(fullname)
    if len(fs):
        file_list.append((directory, fs))

os.path.walk("Volume", getDataFiles, data_files)



# description of what is going to be included in the distribution and
# installed.
from version import VERSION
dist = setup (name = 'Volume',
              version = VERSION,
              description = "Volume Rendering python package",
              author = 'Molecular Graphics Laboratory',
              author_email = 'mgltools@scripps.edu',
              download_url = 'http://www.scripps.edu/~sanner/software/packager.html',
              url = 'http://www.scripps.edu/~sanner/software/index.html',
              packages = packages,
              data_files = data_files,
              cmdclass = {'sdist': modified_sdist,
                          'install_data': modified_install_data,
                          },
              )

