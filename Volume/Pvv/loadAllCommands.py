#############################################################################
#
# Author: Anna Omelchenko
#
# Copyright: M. Sanner TSRI
#
#############################################################################

#
#$Header: /opt/cvs/Volume/Pvv/loadAllCommands.py,v 1.1.1.1 2003/05/23 18:03:19 annao Exp $
#
#$Id: loadAllCommands.py,v 1.1.1.1 2003/05/23 18:03:19 annao Exp $
#
"""Module implements a command that allows to load all available Pvv commands."""
from ViewerFramework.VFCommand import Command, CommandGUI
from mglutil.gui.InputForm.Tk.gui import InputFormDescr
import os


class LoadAll(Command):
    """Command to load all available Python Volume Viewer commands"""
    
    def __init__(self):
          Command.__init__(self)
          self.package = "Volume.Pvv"


    def onAddCmdToViewer(self):
        pass

        
    def guiCallback(self):
        self.doitWrapper()
        

    def doit(self, **kw):
        library='modlib'
        modu = __import__(self.package+'.modlib',
                          globals(), locals(), [library])
        for entry in modu.modlist:
            #print entry[1]
            self.vf.loadModule(entry[1], self.package)


    def __call__(self, **kw):
        apply(self.doitWrapper,(),kw)
    

commandGUI = CommandGUI()
menuName = 'Load All Commands'
menuIndex = 25
commandNames = {'vli':'LoadAll', 'utvolren': 'LoadAll'}
commandList = [{'name':'', 'cmd': None, 'gui':None}]


def initModule(viewer):
    for dict in commandList:
        viewer.addCommand(dict['cmd'], dict['name'], dict['gui'])
             



