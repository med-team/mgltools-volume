## Automatically adapted for numpy.oldnumeric Jul 23, 2007 by 

#############################################################################
#
# Author: Anna Omelchenko
#
# Copyright: M. Sanner TSRI
#
#############################################################################

#
#$Header: /opt/cvs/Volume/Pvv/boundboxCommands.py,v 1.4 2007/07/24 17:30:45 vareille Exp $
#
#$Id: boundboxCommands.py,v 1.4 2007/07/24 17:30:45 vareille Exp $
#
"""
This module implements commands to add and remove a volume bounding box.
"""

from ViewerFramework.VFCommand import Command, CommandGUI
from mglutil.gui.InputForm.Tk.gui import InputFormDescr
from DejaVu.Box import Box
import numpy.oldnumeric as Numeric

volGeom = None

class BoundBox(Command):
    def __init__(self):
        Command.__init__(self)
        
    def __call__(self, **kw):
        self.doitWrapper()

    def guiCallback(self):
        self.doitWrapper()

    def onAddCmdToViewer(self):
        volGeom.onaddVolume_list.append(self)

    def OnAddVolumeToViewer(self):
        if volGeom.updateBoundBox:
            self.updateBox()

    def doit(self):
        """Adds/removes volume bounding box."""
        
        if not volGeom.volume:
            print "Volume is not loaded"
            return
        if not volGeom.boundBox:
            self.addBoundBox()
            val = "Remove"
        else:
            self.removeBoundBox()
            val = "Add"
        self.vf.GUI.VIEWER.Redraw()
        self.updateGUI(val)

    def updateGUI(self, val):
        """Changes the command's menu button to 'Remove Bound Box'
        after adding the box and to 'Add bound box' after removing it."""
        barName = self.GUI.menuDict['menuBarName']
        bar = self.vf.GUI.menuBars[barName]
        entryName = self.GUI.menuDict['menuEntryLabel']
        button = bar.menubuttons[self.GUI.menuDict['menuButtonName'] ]
        menu = button.menu
        newname = val + " Bound Box"
        menu.entryconfig(menu.index(entryName), label = newname)
        self.GUI.menuDict['menuEntryLabel'] = newname

    def showhideBox(self, val):
        box = volGeom.boundBox
        if box:
            if val:
                box.visible=1
            else:
                box.visible=0
            self.vf.GUI.VIEWER.deleteOpenglList()
            self.vf.GUI.VIEWER.Redraw()

    def addBoundBox(self):
        # Has to be implemented by a derived class
        pass

    def removeBoundBox(self):
        vi  = self.vf.GUI.VIEWER    
        vi.RemoveObject(volGeom.boundBox)
        vi.SetCurrentObject(vi.rootObject)
        volGeom.boundBox = None

    def updateBox(self):
        # Has to be implemented by a derived class
        pass

class VLIBoundBox(BoundBox):
    """Command to add/remove a bounding box of VLI volume object."""
    def updateBox(self):
        #print "updating the box"
        box = volGeom.boundBox
        if box:
            vi  = self.vf.GUI.VIEWER 
            vx, vy, vz = volGeom.volume.GetSize()
            box.Set(cornerPoints = ((-vx/2., -vy/2., -vz/2.),
                                                   (vx/2., vy/2., vz/2.)))
            vliScale = volGeom.vliScalemat #VLIMatrix instance
            boxScale = box.scale.tolist()
            d = 0
            if boxScale[0] != vliScale[0][0]:
                boxScale[0] = vliScale[0][0]
                d = 1
            if  boxScale[1] != vliScale[1][1]:
                boxScale[1] = vliScale[1][1]
                d = 1
            if  boxScale[2] != vliScale[2][2]:
                boxScale[2] = vliScale[2][2]
                d = 1
            if d:
                box.SetScale(boxScale)
            vi.Redraw()

    def addBoundBox(self):
        vi  = self.vf.GUI.VIEWER
        vx, vy, vz = volGeom.volume.GetSize()
        box = Box(volGeom.name+'BoundBox', inheritMaterial=0,
                  cornerPoints = ((-vx/2., -vy/2., -vz/2.),
                                  (vx/2., vy/2., vz/2.)))

        vi.AddObject(box)
        sc = Numeric.identity(4).astype('f')
        if volGeom.vliScalemat:
            scale = (volGeom.vliScalemat[0][0],
                     volGeom.vliScalemat[1][1],
                     volGeom.vliScalemat[2][2]) #VLIMatrix instance
        else:
            scale = None
        if volGeom.vliTranslate:
            trans = (volGeom.vliTranslate[0][3],
                     volGeom.vliTranslate[1][3],
                     volGeom.vliTranslate[2][3])#VLIMatrix instance
        else:
            trans = None
        if scale:
            sc[0][0] = scale[0]
            sc[1][1] = scale[1]
            sc[2][2] = scale[2]
            box.SetScale((sc[0][0],sc[1][1], sc[2][2]))
        if trans:
            tr = Numeric.identity(4).astype('f')
            tr[:,3] = [trans[0],trans[1], trans[2],1.0]
            tr = Numeric.dot(sc,tr)
            box.SetTranslation(tr[:3,3])
        volGeom.boundBox = box



class UTBoundBox(BoundBox):
    """Command to add/remove a bounding box of OpenGL based
    ( Univ. of Texas Volume Library) volume object."""
    def addBoundBox(self):

        vi  = self.vf.GUI.VIEWER
        vx, vy, vz = 1, 1, 1
        box = Box(volGeom.name+'BoundBox', inheritMaterial=0,
              cornerPoints = ((-vx/2., -vy/2., -vz/2.), (vx/2., vy/2., vz/2.)))
        vi.AddObject(box)
        volGeom.boundBox = box
        self.setTransformation(box)

    def setTransformation(self, box):
        sc = Numeric.identity(4).astype('f')
        scale = volGeom.scale
        if scale[0] == 1 and scale[1] == 1 and scale[2] == 1:
            scale = None
        trans = volGeom.translation
        if trans[0] == 0 and trans[1] == 0 and trans[2] == 0:
            trans = None
        if scale:
            sc[0][0] = scale[0]
            sc[1][1] = scale[1]
            sc[2][2] = scale[2]
            box.SetScale((sc[0][0],sc[1][1], sc[2][2]))
        if trans:
            box.SetTranslation(trans)



    def updateBox(self):
        #print "updating the box"
        box = volGeom.boundBox
        print "box: ", box
        if box:
            vi  = self.vf.GUI.VIEWER 
            vx, vy, vz = 1, 1, 1
            box.Set(cornerPoints = ((-vx/2., -vy/2., -vz/2.),
                                                   (vx/2., vy/2., vz/2.)))
            self.setTransformation(box)
            vi.Redraw()


commandGUI = CommandGUI()
menuName = 'Add Bounding Box'
menuIndex = 3
commandNames = {'vli':'VLIBoundBox', 'utvolren': 'UTBoundBox'}
commandList = [{'name': '', 'cmd': None, 'gui': commandGUI}]


          
def initModule(viewer):
    for dict in commandList:
        if dict['name']:
            viewer.addCommand(dict['cmd'], dict['name'], dict['gui'])







