#############################################################################
#
# Author: Anna Omelchenko
#
# Copyright: M. Sanner TSRI
#
#############################################################################

#
#$Header: /opt/cvs/Volume/Pvv/showhideCommands.py,v 1.1.1.1 2003/05/23 18:03:19 annao Exp $
#
#$Id: showhideCommands.py,v 1.1.1.1 2003/05/23 18:03:19 annao Exp $
#
"""Module implements a command to show/hide the volume object in the viewer."""
from ViewerFramework.VFCommand import Command, CommandGUI
from mglutil.gui.InputForm.Tk.gui import InputFormDescr
import os
volGeom = None

class ShowHide(Command):
    """Command to show/hide the volume object"""
    
    def __init__(self):
          Command.__init__(self)

    def onAddCmdToViewer(self):
        pass

        
    def guiCallback(self):
        """Called every time 'show/hide volume' button is pressed"""
        val = not volGeom.visible
        self.doitWrapper(val)


    def doit(self, visible, **kw):
        """Makes the volume visible/unvisible"""
        if volGeom.volume == None: return
        volGeom.Set(visible = visible)
        self.vf.GUI.VIEWER.Redraw()
        self.customizeGUI()

    def __call__(self, visible, **kw):
        """None <- ShowHide(visible, **kw)
        Shows/hides the volume.
        visible=1 - show volume;
        visible=0 - hide volume; """

        apply(self.doitWrapper, (visible,),kw)
    

    def customizeGUI(self):
        """Changes the menu button to 'Hide' when the volume is
        visible and to 'Show' when the volume is unvisible."""
        if volGeom.volume == None: return
        barName = self.GUI.menuDict['menuBarName']
        bar = self.vf.GUI.menuBars[barName]
        entryName = self.GUI.menuDict['menuEntryLabel']
        buttonName = self.GUI.menuDict['menuButtonName']
        button = bar.menubuttons[buttonName]
        n = button.menu
        if volGeom.visible:
            newname = 'Hide volume'
        else:
            newname = 'Show volume'
        n.entryconfig(n.index(entryName), label = newname )
        self.GUI.menuDict['menuEntryLabel'] = newname

commandGUI = CommandGUI()
menuName = 'Show/Hide volume'
menuIndex = 5
commandNames = {'vli':'ShowHide', 'utvolren': 'ShowHide'}
commandList = [{'name': '', 'cmd': None, 'gui': commandGUI}]


def initModule(viewer):
    for dict in commandList:
        if dict['name']:
            viewer.addCommand(dict['cmd'], dict['name'], dict['gui'])
             

