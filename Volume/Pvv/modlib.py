modlist = [
("Volume.Pvv","startPvvCommands","""None"""),
("Volume.Pvv","loadAllCommands","""Module implements a command that allows to load all available Pvv commands."""),
("Volume.Pvv","boundboxCommands","""
This module implements commands to add and remove a volume bounding box.
"""),
("Volume.Pvv","cropCommands","""None"""),
("Volume.Pvv","cursorCommands","""None"""),
("Volume.Pvv","cutplaneCommands","""None"""),
#("Volume.Pvv","getimageCommands","""None"""),
("Volume.Pvv","getsubvolCommands",""" Module implements a command to get a subvolume from a displayed volume and
to load it in to the viewer as a separate volume.
GUI description:
    Cropping box (CB) is used for selecting the subvolume size:
        'Control' + mouse buttons 1,2,3 are used to change the CB size
        along the X, Y, Z axes, respectively;
        'meta' + mouse button 3 changes the CB size in all directions;
        'meta' + button 1 moves the CB inside the volume.
        
    Press ' >> ' button to get a subvolume,
    Press ' << ' button to return to the original volume.
    Press 'volume outline' checkbutton to display the outline
    of the original (parent) volume.
    To switch to another parent volume use 'Dismiss' button followd by
    'Get Subvolume' from the main menu. """),
("Volume.Pvv","gridToVolumeCommands","""Module implements a command to create a volume from autogrid data that
has been read by Pmv gridCommands or specified .map file, display the volume
or save it into a file.
GUI description:
    The user can select the type of data scaling(linear or logarithmic)
    by pressing the corresponding radio button and specify the mapping
    intervals by typing in the provided entry fields.
    The actual data has to be mapped to scalars in range
    [scalar_start<=scalar_min,scalar_max<=scalar_limit].
    If mapping intervals are specified by supplying data min (greater or 
    equal to minimum array value) and data max (less or equal to maximum
    array value) values and scalar min and max, then the mapping will
    proceed as follows:
        [data_min,data_max] is maped to [scalar_min,scalar_max].
    By pressing 'New grid to volume' button the user can obtain a new
    input form (GUI) with data information for converting another AutoGrid
    object or .map data file into a volume."""),
("Volume.Pvv","lightCommands","""Module implements a command to add, remove and change the direction of
one or more light sources. A light direction is specified in
world space. A light intensity is set to 1 (full intensity).
The GUI consists of eight check buttons corresponding to
a similar set of buttons of the DejaVu GUI that allow
the user to select a light source,
a switch to turn selected light on/off and
a combobox with a dropdown listbox containing the light sources
direction info. Picking one of the lists entries
selects a corresponding light source.
A set of entry fields can be used to specify the current light direction.
The current light direction can be set interactively using the mouse by
redirecting the mouse transformation to the current light source in
DejaVu Viewer GUI ('showHideDejaVuGUI' from DejaVu Macro library).
"""),

("Volume.Pvv","numarrtovolCommands","""Module implements a command that converts a Numeric Array to volumetric
data and loads the volume into the viewer."""),
("Volume.Pvv","showhideCommands","""Module implements a command to show/hide the volume object in the viewer."""),
("Volume.Pvv","transferCommands","""None"""),
("Volume.Pvv","vlioptionsCommands","""This module exposes a number of VLI features (gradients, blend mode,
render mode, supersampling factors) that allow the application
to control the rendering context and appearance of the volume"""),
("Volume.Pvv","voltransformCommands","""Module implements a command to allow the user to scale and translate
the volume geometry object along X, Y and Z axes.
The command sets the modeling transformation matrix.
The GUI: The user can choose to scale or translate
the volume object by pressing a corresponding radio button.
The amount to scale or move the object in each direction can
be specified either by typing X, Y, Z values in the provided entry fields
or by rotating the thumb wheels."""),
("Volume.Pvv","zoomvolumeCommands","""Module implementing a command that enables the user to navigate
through large data volumes by choosing a subvolume from a lower
resolution dataset (with a crop box or a geometry box) followed by
loading (zoomming in) that part of data from a higher resolution
dataset into the viewer as a new volume.
The command uses VisTools Mesh API wrapped in Python.
The datasets file names, the lowest resolution number, the cache and
page sizes have to be provided by the user in a separate text file(currently
with .data extension).
Current limitations:
- the lowest resolution number must be a power of two;
- the resolution ratio between two consequent datafiles has
 to be equaled to 2;
- the number of datafiles must correspond to the given lowest
 resolution (e.g., if lowest_resolution == 8 then number_files = 4);
- the datafile names must be given in the resolution increasing order ,
 starting from the file with the lowest resolution(no order check is done);
- the data length must be 8 bit;
- .vols datafile format is supported (SDSC 'VOL' volume file format;
  info at http://vistools.npaci.edu/Documentation/C++Doc/Formats/index.html)
An example of .data file:
Lowest Resolution 8
File brain_stride_8_int.vols
Page Size 14803
Cache Size 1391485
File brain_stride_4_int.vols
Page Size 59474
Cache Size 11181112
File brain_stride_2_int.vols
Page Size 238875
Cache Size 30000000
File brain_full_size_int.vols
Page Size 1048576
Cache Size 64000000
scale 1.5 1.0 1.0
"""),
]
