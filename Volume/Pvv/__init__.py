#__init__.py
                         
volGeom = None
menuTitles = {'vli':'VLI Volume Renderer', 'utvolren':'UT Volume Renderer'}
cmdlist = []
from Volume.Renderers import ChooseRenderer
from Volume.Renderers import istest, renderer
print "renderer = : ", renderer, "istest: ", istest
if istest:
    flag, geomdict = ChooseRenderer(renderer)
else:
    flag, geomdict = ChooseRenderer()
    
if flag:
    volGeom = geomdict[flag]
    from modlib import modlist
##      modlist = ['boundboxCommands', 'showhideCommands', 'cropCommands',
##                 'cursorCommands', 'cutplaneCommands',  'getsubvolCommands',
##                 'numarrtovolCommands', 'loadAllCommands', 'lightCommands',
##                 'startPmvCommands', 'gridToVolumeCommands', 'vlioptionsCommands',
##              'transferCommands', 'voltransformCommands', 'zoomvolumeCommands']
    for mod in modlist:
        module_str = mod[1]
        #module_str = mod
        module = __import__(module_str, globals(), locals(), [])
        module.volGeom = volGeom
        menuName = module.menuName
        menuIndex = module.menuIndex
        name= module.commandNames[flag]
        menuTitle = menuTitles[flag]
        if name:
            if module.commandGUI:
                module.commandGUI.addMenuCommand('menuRoot', menuTitle,
                                         menuName, index=menuIndex)
            module.commandList = [{'name': name,
                              'cmd': eval(module_str+"."+name+"()"),
                              'gui':module.commandGUI}]

            cmdlist.append(name)



def initModules(renderer):
    # used for testing 
    global cmdlist
    cmdlist = []
    flag, geomdict = ChooseRenderer(renderer)
    if flag:
        global volGeom
        volGeom = geomdict[flag]
        from modlib import modlist
        for mod in modlist:
            module_str = mod[1]
            #module_str = mod
            module = __import__(module_str, globals(), locals(), [])
            module.volGeom = volGeom
            menuName = module.menuName
            menuIndex = module.menuIndex
            name= module.commandNames[flag]
            menuTitle = menuTitles[flag]
            if name:
                if module.commandGUI:
                    module.commandGUI.addMenuCommand('menuRoot', menuTitle,
                                             menuName, index=menuIndex)
                module.commandList = [{'name': name,
                                  'cmd': eval(module_str+"."+name+"()"),
                                  'gui':module.commandGUI}]

                cmdlist.append(name)


