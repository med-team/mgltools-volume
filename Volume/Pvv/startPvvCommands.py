#############################################################################
#
# Author: Anna Omelchenko
#
# Copyright: M. Sanner TSRI
#
#############################################################################

#
#$Header: /opt/cvs/Volume/Pvv/startPvvCommands.py,v 1.3 2004/06/24 18:00:01 annao Exp $
#
#$Id: startPvvCommands.py,v 1.3 2004/06/24 18:00:01 annao Exp $
#
from ViewerFramework.VFCommand import Command, CommandGUI
from mglutil.gui.InputForm.Tk.gui import InputFormDescr
volGeom = None

import os, string

class StartVolumeRenderer(Command):
    """Adding this command to the viewer prepares the environment for
       creating and rendering a volume. It also adds 'Volume Renderer'
       button to the main menu and 'Read .vox' button to the drop-down
       menu under 'Volume Renderer' to enable loading a volume from
       a data file."""
    
    def __init__(self):
          Command.__init__(self)
          self.lastDir = "."
        
    def guiCallback(self, *args, **kw):
        """Pops up a file browser to specify a volume
        file."""
        file = self.vf.askFileOpen(types=self.fileTypes,
                                   idir = self.lastDir,
                                   title=self.fileBrowserTitle)
	vol = None
	if file :
            self.lastDir = os.path.split(file)[0]
            args = (file,)
            #kw['busyIdle'] = 1
            vol = apply( self.doitWrapper, args, kw )
        return vol

    def doit(self,filename, **kw):
        """Loads the volume. """

        volGeom.LoadVolume(filename)
        #self.vf.GUI.busy(self.busyCursor)
        self.vf.GUI.VIEWER.Redraw()


class StartVLI(StartVolumeRenderer):
    """Class to initialize a volume renderer based on VLI API."""
    
    def __init__(self):
        StartVolumeRenderer.__init__(self)
        print "volGeom: ", volGeom
        self.fileTypes = [('Vox Files', '*.vox')]
        self.fileBrowserTitle = "read VOX file:"

    def onAddCmdToViewer(self):
        """Called when the command is loaded (added) to the viewer.
        Sets the viewer's projection type to orthographic.
        Creates mouse/keyboard bindings to interactivly manipulate
        the cropping of the volume. """
        
        vi = self.vf.GUI.VIEWER
        volGeom.viewer = vi
        volGeom.masterObject = vi.rootObject
        c=vi.currentCamera
        c.Set(projectionType=c.ORTHOGRAPHIC)
        c.fog.Set(enable=0)
        volGeom.Set(volscale =(1., 1., 1.))
        vi.AddObject(volGeom)

        c.actions['Light']['rotation'] = volGeom.RotateVLILight
        self.initLighting()
        vi.GUI.lightOnOffButton.configure(command = volGeom.VLILightOnOff)
        cropBox = volGeom.cropBox
        c.actions['Object']['cropScaleX'] = cropBox.ScaleX
        c.actions['Object']['cropScaleY'] = cropBox.ScaleY
        c.actions['Object']['cropScaleZ'] = cropBox.ScaleZ
        c.actions['Object']['cropTransX'] = cropBox.TranslationX
        c.actions['Object']['cropTransY'] = cropBox.TranslationY
        c.actions['Object']['cropTransZ'] = cropBox.TranslationZ
        c.actions['Object']['cropScaleAll'] = cropBox.ScaleAll
        c.actions['Object']['cropTransAll'] = volGeom.translateCropbox

        volGeom.BindMouseToCropScale()
        volGeom.BindMouseToCropTransAll()
            
        self.vf.libraries.append('Volume.Pvv')


    def initLighting(self):
        """Initializes the vli lighting."""
        
        from Volume.Renderers.VLI import vli
        vi = volGeom.viewer
        for l in vi.lights:
            #dir = l.direction*(-1)
            ldir = l.direction
            vec=vli.VLIVector3D(ldir[0]*(-1), ldir[1]*(-1),ldir[2]*(-1))
            light = vli.VLILight_CreateDirectional(vec)
            volGeom.lights.append(light)
        currnum = int(string.split(vi.currentLight.name)[1])
        currLight = volGeom.lights[currnum]
        volGeom.context.AddLight(currLight)
        volGeom.currLight=currLight


class StartUTVolRen(StartVolumeRenderer):
    """Class to initialize OpenGL based Volume Renderer (Univ. of Texas)."""
    
    def __init__(self):
        StartVolumeRenderer.__init__(self)
        print "volGeom: ", volGeom
        self.fileTypes = [('Raw data files', '*.rawiv')]
        self.fileBrowserTitle = "read Data File:"
        

    def onAddCmdToViewer(self):
        """Called when the command is loaded (added) to the viewer.
        Creates mouse/keyboard bindings to interactivly manipulate
        the cropping of the volume. """
        
        vi = self.vf.GUI.VIEWER
        #volGeom.viewer = vi # is done by AddObject !
        volGeom.masterObject = vi.rootObject

        vi.AddObject(volGeom)

        vi.cameras[0].Activate()
        cropBox = volGeom.cropBox
        c=vi.currentCamera
        c.actions['Object']['cropScaleX'] = cropBox.ScaleX
        c.actions['Object']['cropScaleY'] = cropBox.ScaleY
        c.actions['Object']['cropScaleZ'] = cropBox.ScaleZ
        c.actions['Object']['cropTransX'] = cropBox.TranslationX
        c.actions['Object']['cropTransY'] = cropBox.TranslationY
        c.actions['Object']['cropTransZ'] = cropBox.TranslationZ
        c.actions['Object']['cropScaleAll'] = cropBox.ScaleAll
        c.actions['Object']['cropTransAll'] = volGeom.translateCropbox

        volGeom.BindMouseToCropScale()
        volGeom.BindMouseToCropTransAll()

        if not volGeom.volrenInitialized:
            volGeom.InitVolumeRenderer()
        self.vf.libraries.append('Volume.Pvv')




        
commandGUI = CommandGUI()

menuName = 'Read file'
menuIndex = 1
##  addMenuArgs = {'vli':"('menuRoot', 'start VLI', 'Read file', index=1)",
##         'utvolren': "('menuRoot', 'Volume Renderer', 'Read file', index = 1)"}
commandNames = {'vli':'StartVLI', 'utvolren': 'StartUTVolRen'}
commandList = [{'name': None, 'cmd': None,
                  'gui':None}]


             
def initModule(viewer):
    for dict in commandList:
        viewer.addCommand(dict['cmd'], dict['name'], dict['gui'])





